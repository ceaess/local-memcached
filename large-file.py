from pymemcache.client.base import Client


f = open('testfile.txt', 'r')
client = Client(('localhost', 11211))


def set_file(name, f):
    chunk_number = 0
    while True:
        # Bug here with reading
        # more chunks than are in the file
        chunk = f.read(1000000)
        if chunk:
            client.set(name + str(chunk_number), {
                "data": chunk
            })
            chunk_number += 1
        else:
            client.set(name + str(chunk_number), {
                # TODO: find better terminator indicator
                'data': 'END'
            })
            break


def get_file(name):
    f = open(name + 'output', 'w')
    chunk_number = 0
    while True:
        result = client.get(name + str(chunk_number))
        # TODO: find better terminator indicator
        if result is not 'END' and result is not None:
            f.write(result)
            chunk_number += 1
        else:
            break

    return f


set_file('temp_file', f)
test = get_file('temp_file')

f.close()



