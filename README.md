# Running locally:

1. Ensure docker is installed and running on your system.
1. Ensure `docker` is installed in system default python
   (an update to this repo will ensure ansible calls virtualenv python,
   but I ran out of time to do this)
1. Activate a new virtualenv with python >=3.5
1. `pip install -r requirements.txt`
1. Run `ansible-playbook ansible-memcached.yml`
1. To check that everything's dandy, `docker ps` should display a
   container called local-memcache running.
1. Run & modify the `test-memcached-connection.py` to experiment with
   local memcached
