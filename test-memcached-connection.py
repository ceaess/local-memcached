from pymemcache.client.base import Client

client = Client(('localhost', 11211))
client.set('Howdy', 'Hi Bryan & James!')
result = client.get('Howdy')
print(result.decode('UTF8'))
